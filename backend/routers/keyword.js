const express = require('express');
const router = express.Router();
router.use(express.json());

const findKeyword = (search, jsonData, group) => {
    const categories = Object.keys(jsonData);
    const matchedKeywords = [];

    for (const category of categories) {
        for (const item of jsonData[category]) {
            const keyword = item['Keyword'];
            if (!group) {
                if (search.includes(keyword)) {
                    matchedKeywords.push({ category, keyword });
                }
            } else {
                if (search.includes(keyword) && category === group) {
                    matchedKeywords.push({ category, keyword });
                }
            }
        }
    }
    return matchedKeywords;
};

const countCategory = (results) => {
    const categoryCount = {};
    results.forEach(({ category }) => {
        categoryCount[category] = (categoryCount[category] || 0) + 1;
    });
    return categoryCount;
};

router.post('/', (req, res) => {
    const { search, group, keyword: jsonData } = req.body;

    if (!search) {
        return res.status(400).send("Input data is required.");
    }

    try {
        const results = findKeyword(search, jsonData, group);
        if (results.length > 0) {
            res.status(200).json({
                input: search,
                count: results.length,
                categorycount: countCategory(results),
                results: results,
            });
        } else {
            res.status(200).json({
                input: search,
                count: "0",
                categorycount: countCategory(results),
                message: "No match found"
            });
        }
    } catch (error) {
        console.error('Error processing data:', error);
        res.status(500).send('Server error');
    }
});

module.exports = router;
