const express = require('express');
const app = express();
const cors = require('cors');
const morgan = require('morgan');

//ROUTERS PATH
const keywords = require('./routers/keyword'); 

// MIDDLEWARE
app.use(cors());
app.use(morgan('tiny'));
app.use(express.json()); 

// USER ROUTERS
app.use('/api/searchkeyword', keywords);


// SERVER
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`✅ Server is running on http://localhost:${PORT}`);
});

module.exports = app;
